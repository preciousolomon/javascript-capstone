var enterButton = document.getElementById('enter');
var input = document.getElementById('userInput');
var ul = document.querySelector('ul');
var item = document.getElementsByTagName('li');

function inputLength(){
    return input.value.length;
}

function listLength(){
    return item.length;
}

function createListElement(){
var li = document.createElement("li"); //create new item in list
li.appendChild(document.createTextNode(input.value)); //makes li text from input
ul.appendChild(li); // adds li to ul
input.value = ""; //reset text input field

//strikethrough
function crossOut() {
    li.classList.toggle("done");
}

li.addEventListener('click', crossOut);
//strikethrough

//delete btn
var deleteBtn = document.createElement('button');
    deleteBtn.appendChild(document.createTextNode("X"));
    li.appendChild(deleteBtn);  
    deleteBtn.addEventListener('click', deleteListItems);

function deleteListItems(){
    li.classList.add('delete')
}
//delete btn
}
//Adds items to list
function addlistAfterClick(){
    if(inputLength() > 0 ){  //prevent empty input added to list 
        createListElement();
    }
}   
//

//add items when you hit enter
function addListAfterKeypress(event){
    if(inputLength() > 0 && event.which === 13) { //13-enterkey keycode, checks if you hit enter
    createListElement();
    
    } 
}

enterButton.addEventListener('click', addlistAfterClick);
input.addEventListener('keypress', addListAfterKeypress);


